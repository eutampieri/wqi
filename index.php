<?php
$iman="206624415";
$eugenio="68870771";
if(!isset($_GET["page"])){
	$_GET["page"]="home";
}
function benvenuto($n){
	/*$genderCache=json_decode(file_get_contents("res/genCache.json"),true);
	if(!isset($genderCache[explode(" ", $n)[0]])){
		$gender=json_decode(file_get_contents("https://gender-api.com/get?key=JLDzyuUuJsueSlVyEE&name=".urlencode(explode(" ", $n)[0])),true)["gender"];
		$genderCache[explode(" ", $n)[0]]=$gender;
		error_log("Nuovo utilizzo genderAPI");
		file_put_contents("res/genCache.json", json_encode($genderCache));
	}
	else{
		$gender=$genderCache[explode(" ", $n)[0]];
	}*/
	$gender=json_decode(file_get_contents("https://api.etsrv.tk/gender/?name=".urlencode(explode(" ", $n)[0])),true);
	if($gender=="female"){
		return "Benvenuta, ".$n.". Sarai disconnessa alle ";
	}
	return "Benvenuto, ".$n.". Sarai disconnesso alle ";
}
function pagePath($page){
	$path="res/pagine/".$page.".html";
	if(is_file($path)){
		return $path;
	}
	else{
		return "res/pagine/".$page.".php";
	}
}
function dataIT($data)
{
	$data = explode(" ", $data) [0];
	$data = explode("-", $data);
	return $data[2] . "/" . $data[1] . "/" . $data[0];
}
function oraIT($data)
{
	return explode(" ", $data) [1];
}
function notify($to,$txt){
    file_get_contents("https://api.telegram.org/bot364276959:AAEYrHeqzKtF6bliN-juXWEjTf3WRg357Mw/sendMessage?text=".urlencode($txt)."&chat_id=".strval($to));
}
$createTables=false;
if(!is_file("dati.sqlite")){
	$createTables=true;
}
$database = new PDO('sqlite:dati.sqlite');
$database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if($createTables){
	$qry="CREATE TABLE 'Misure' ('ID' TEXT UNIQUE, 'Misura' REAL, 'Tipo' TEXT, 'Utente' TEXT, 'Data' INTEGER, 'Posizione' TEXT, 'Interpolata' REAL);";
	$stmt = $database->prepare($qry);
	$stmt->execute();
	$qry="CREATE TABLE 'Utenti' ( 'Utente' TEXT UNIQUE, 'Nome' TEXT, 'Password' TEXT, 'Autorizzazzioni' TEXT)";
	$stmt = $database->prepare($qry);
	$stmt->execute();
	$qry="CREATE TABLE 'Sessioni' ( 'Token' TEXT UNIQUE, 'Utente' TEXT, 'Scadenza' TEXT);";
	$stmt = $database->prepare($qry);
	$stmt->execute();
	$qry="CREATE TABLE 'Foto' ( 'ID' TEXT UNIQUE, 'Utente' TEXT, 'Foto' BLOB);";
	$stmt = $database->prepare($qry);
	$stmt->execute();
}
$loggedIn=false;
$scad="";
if(isset($_COOKIE['token'])){
	$qry='SELECT * FROM Sessioni WHERE Token = :tk';
	$stmt = $database->prepare($qry);
	$stmt->bindParam(':tk',$_COOKIE['token']);
	$stmt->execute();
	$sess=$stmt->fetchAll(PDO::FETCH_ASSOC)[0];
    $scad=oraIT($sess["Scadenza"]);
	if(date_timestamp_get(date_create_from_format("Y-m-d H:i:s",$sess["Scadenza"]))>time()){
		$qry='SELECT * FROM Utenti WHERE Utente = :u';
		$stmt = $database->prepare($qry);
		$stmt->bindParam(':u',$sess["Utente"]);
		$stmt->execute();
		$udata=$stmt->fetchAll(PDO::FETCH_ASSOC)[0];
		$nome=$udata["Nome"];
          $uname=$sess["Utente"];
		$loggedIn=true;
	    $username=$udata["Utente"];
	    $autorizzazioniUtente=json_decode($udata["Autorizzazzioni"],true);
	}
	else{
		header("Location: index.php?error=Sessione+scaduta&page=login");
	}
}
if($loggedIn&&$_GET["page"]=="login"){
	header("Location: index.php?page=dati");
}
if(!$loggedIn&&$_GET["page"]=="dati"){
	header("Location: index.php?page=login");
}
if(!isset($_POST["action"])){
    $_POST["action"]="";
}
switch ($_POST["action"]) {
	case 'login':
		$qry='SELECT * FROM Utenti WHERE Utente = :u';
		$stmt = $database->prepare($qry);
		$stmt->bindParam(':u',$_POST["utente"]);
		$stmt->execute();
		$utenti=$stmt->fetchAll(PDO::FETCH_ASSOC);
		if(count($utenti)==1){
			if(password_verify($_POST["password"],$utenti[0]["Password"])){
				$qry='INSERT INTO Sessioni VALUES (:token, :utente, :scadenza)';
				$token=strval(uniqid("sess"));
				$scadenza=date("Y-m-d H:i:s",time()+8600);
				$stmt = $database->prepare($qry);
				$stmt->bindParam(':token',$token);
				$stmt->bindParam(':scadenza',$scadenza);
				$stmt->bindParam(':utente',$_POST["utente"]);
				$stmt->execute();
				setcookie("token",$token, time()+8600);
				header("Location: index.php?page=dati");
			}
			else{
                notify($eugenio,"Login fallito, password errata. Utente: ".$_POST["utente"].", IP: ".$_SERVER['REMOTE_ADDR'].", ".$_SERVER["HTTP_CF_CONNECTING_IP"].", da ".$_SERVER["HTTP_CF_IPCOUNTRY"].", RAY: ".$_SERVER["HTTP_CF_RAY"]);
				header("Location: index.php?page=login&error=Password+errata");
			}
		}
		else{
            notify($eugenio,"Login fallito, utente inesistente. Utente: ".$_POST["utente"].", Password: ".$_POST["password"]." IP: ".$_SERVER['REMOTE_ADDR'].", ".$_SERVER["HTTP_CF_CONNECTING_IP"].", da ".$_SERVER["HTTP_CF_IPCOUNTRY"].", RAY: ".$_SERVER["HTTP_CF_RAY"]);
			header("Location: index.php?error=Utente+inesistente&page=login");
            
		}
		die();
		break;
    case "invia":
	if(in_array($_POST["dato"],$autorizzazioniUtente)){
        notify($iman,"Nuovo dato da interpolare: ".$_POST["dato"]);
        notify($eugenio,"Nuovo dato da interpolare");
        $tipo=$_POST["dato"];
        $valore=floatval(str_replace(",",".",$_POST["valore"]));
        $id=uniqid("mis");
        $data=time();
        $pos=$_POST["posizione"];
        $qry="INSERT INTO Misure VALUES (:id, :valore, :tipo, :utente, :data, :pos, NULL)";
        $stmt = $database->prepare($qry);
        $stmt->bindParam(':id',$id);
        $stmt->bindParam(':tipo',$tipo);
        $stmt->bindParam(':valore',$valore);
        $stmt->bindParam(':data',$data);
        $stmt->bindParam(':pos',$pos);
		$stmt->bindParam(':utente',$uname);
		$stmt->execute();}else{notify($eugenio,$nome." ha tentato di inserire un dato per il quale non era autorizzato: ".$tipo);}
	    case "interp":
	    if(in_array("interp",$autorizzazioniUtente)){
        $valore=floatval(str_replace(",",".",$_POST["valore"]));
        $id=$_POST["interp"];
        $qry="UPDATE Misure SET Interpolata= :valore WHERE ID= :id";
        $stmt = $database->prepare($qry);
        $stmt->bindParam(':id',$id);
        $stmt->bindParam(':valore',$valore);
		$stmt->execute();}
	default:
		# code...
		break;
}
if(isset($_GET["slow"])){
    setcookie("slow","true");
    $_COOKIE["slow"]="true";
}
if(isset($_GET["noslow"])){
    setcookie("slow","true",1);
    unset($_COOKIE["slow"]);
}
?>
<html>
<head>
	<meta charset="utf-8">
	<title>
		LIM
	</title>
    <?php if(!isset($_COOKIE["slow"])){echo'<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">';}?>
    <?php
    if(isset($_COOKIE["slow"])){
        echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
        echo '<link rel="stylesheet" href="res/css/slow.css" />';
    }?>
    <!--meta name="apple-mobile-web-app-capable" content="yes"-->
    <link rel="apple-touch-icon" sizes="180x180" href="icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="icons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="icons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="icons/manifest.json">
    <link rel="mask-icon" href="icons/safari-pinned-tab.svg" color="#ba0001">
    <link rel="shortcut icon" href="icons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="LIM">
    <meta name="application-name" content="LIM">
    <meta name="msapplication-TileColor" content="#b91d47">
    <meta name="msapplication-TileImage" content="icons/mstile-144x144.png">
    <meta name="msapplication-config" content="icons/browserconfig.xml">
    <meta name="theme-color" content="#ba0001">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php if(!isset($_COOKIE["slow"])){echo'<link rel="stylesheet" href="res/css/jquery.mobile-1.4.5.min.css" />';}?>
	<link rel="stylesheet" href="res/css/stile.css" />
	<?php if(!isset($_COOKIE["slow"])){echo'<script src="res/js/jquery-1.11.1.min.js"></script>';}?>
	<?php if(!isset($_COOKIE["slow"])){echo'<script type="text/javascript">
	$(document).bind("mobileinit", function () {
		$.mobile.ajaxEnabled = false;
	});
	</script>
	<script src="res/js/jquery.mobile-1.4.5.min.js"></script>';}?>
	<!--script src="res/js/sha512.js"></script>
	<script src="res/js/bibliodb.js"></script-->
	<script>
        function GPS(){
            if (navigator.geolocation){
                navigator.geolocation.getCurrentPosition(function(position) {document.getElementById("posizione").value=position.coords.latitude+","+position.coords.longitude;});
            }
            else{
                document.getElementById("posizione").value="---NON DISPONIBILE---";
            }
        }
		function calcAlcalinita() {
			var molHCl = parseFloat(document.getElementById("molHCl").value.replace(",","."));
			var volHCl = parseFloat(document.getElementById("volHCl").value.replace(",","."));
			var acqua = parseFloat(document.getElementById("acqua").value.replace(",","."));
			//moli=vol*mol
			var conc = (molHCl*volHCl*500.0/acqua*100.087).toFixed(3);
			document.getElementById("alc").value=conc.toString();
            if(document.getElementById("posizione").value==""){
                GPS();
            }
			if(document.getElementById("molHCl").value!=""&&document.getElementById("volHCl").value!=""&&document.getElementById("acqua").value!=""){
				document.getElementById("alcBtn").disabled=false;
			}
			else{
				document.getElementById("alcBtn").disabled=true;
			}
            <?php if(!isset($_COOKIE["slow"])){echo"$('[type=\"submit\"]').button('refresh');";}?>
		}
		function delta(a,b,c) {
			var temp = parseFloat(document.getElementById(a).value.replace(",","."));
			var temp1 = parseFloat(document.getElementById(b).value.replace(",","."));
			var conc= (temp-temp1).toFixed(3);
			document.getElementById(c).value=conc.toString();
            if(document.getElementById("posizione").value==""){
                GPS();
            }
			if(document.getElementById(a).value!=""&&document.getElementById(b).value!=""){
				document.getElementById("alcBtn").disabled=false;
			}
			else{
				document.getElementById("alcBtn").disabled=true;
			}
			<?php if(!isset($_COOKIE["slow"])){echo"$('[type=\"submit\"]').button('refresh');";}?>
		}
		function media(a,b,c) {
			var temp = parseFloat(document.getElementById(a).value.replace(",","."));
			var temp1 = parseFloat(document.getElementById(b).value.replace(",","."));
			var conc= ((temp+temp1)/2).toFixed(3);
			document.getElementById(c).value=conc.toString();
            if(document.getElementById("posizione").value==""){
                GPS();
            }
			if(document.getElementById(a).value!=""&&document.getElementById(b).value!=""){
				document.getElementById("alcBtn").disabled=false;
			}
			else{
				document.getElementById("alcBtn").disabled=true;
			}
			<?php if(!isset($_COOKIE["slow"])){echo"$('[type=\"submit\"]').button('refresh');";}?>
		}
		function checkEmpty() {
            if(document.getElementById("posizione").value==""){
                GPS();
            }
			if(document.getElementById("alc").value!=""){
				document.getElementById("alcBtn").disabled=false;
			}
			else{
				document.getElementById("alcBtn").disabled=true;
			}
			<?php if(!isset($_COOKIE["slow"])){echo"$('[type=\"submit\"]').button('refresh');";}?>
		}
        function checkEmptyNoGPS() {
			if(document.getElementById("alc").value!=""){
				document.getElementById("alcBtn").disabled=false;
			}
			else{
				document.getElementById("alcBtn").disabled=true;
			}
			<?php if(!isset($_COOKIE["slow"])){echo"$('[type=\"submit\"]').button('refresh');";}?>
		}
		function prop(a,b,d,dest){
			//a:b=c:d
			va=parseFloat(document.getElementById(a).value.replace(",","."));
			vb=parseFloat(document.getElementById(b).value.replace(",","."));
			res= va*d/vb;
			document.getElementById(dest).value=res.toFixed(3).toString();
		}
        function cmtoft(a){
			//a:b=c:d
			va=parseFloat(document.getElementById(a).value.replace(",","."))*0.0328;
			document.getElementById(a).value=va.toFixed(3).toString();
		}
	</script>
</head>
<body>
	<?php
		$pgname=$_GET["page"];
		$titoli=json_decode(file_get_contents("res/titoli.json"),true);
	?>
	<div data-role="page" data-control-title="Home" id="page1">
		<div data-theme="a" class="redHeader" data-role="header" data-position="fixed">
			<h1>
				<?php if(!isset($_COOKIE["slow"])){echo'<img class="logoSmall" src="res/imgs/logo.png">';}?>
				<?php if(isset($titoli[$pgname])){echo $titoli[$pgname];}else{echo ucwords($pgname);} ?>
			</h1>
		</div>
		<div data-role="content">

		<?php
		if($loggedIn){
			echo "<div class=\"benvenuto\"><span class=\"benvtxt\">".benvenuto($nome).$scad."</span></div>\n";
		}
		include(pagePath($pgname));
		echo "\n";
            if(isset($_COOKIE["slow"])){echo '<a href="?noslow" data-transition="fade" data-theme="" class="isSlow" data-icon="cloud">
						Ho una connessione veloce
					</a>';}
		?>
		</div>
		<div data-role="footer" data-position="fixed">
			<div data-role="navbar" data-iconpos="top" data-theme="a">
				<ul>
					<li>
						<a  href="index.php" data-transition="fade" data-theme="" data-icon="home">
							Home
						</a>
					</li>
					<li>
					<a href="?page=login" data-transition="fade" data-theme="" data-icon="lock">
						Area riservata
					</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>
