<?php
$titFrm=json_decode(file_get_contents("res/forms.json"),true);
$units=json_decode(file_get_contents("res/units.json"),true);
if(!$loggedIn){
    header("Location: index.php?page=login");
    die();
}
if(isset($_GET["dato"])){
    $qry='SELECT * FROM Misure WHERE ID = :id';
    $stmt = $database->prepare($qry);
    $stmt->bindParam(':id',$_GET["dato"]);
    $stmt->execute();
    $dato=$stmt->fetchAll(PDO::FETCH_ASSOC)[0];
    if($dato["Interpolata"]==null){
	//Maschera interpolazione
    echo'<h1>Interpolazione '.$titFrm[$dato["Tipo"].".html"].'</h1>';
        if($dato["Tipo"]=="torbidita"){
            echo "<div class='errore'>1 ft = 12 in</div>";
        }
        echo '<form method="POST" action="index.php">
    Dato grezzo: '.str_replace(".",",",$dato["Misura"])." ".$units[$dato["Tipo"]].'<hr>
    Dato interpolato (nessuna unit&agrave; di misura, 0 se non contemplato nell\'indice <i>GREEN</i>):
    <input type="hidden" name="action" value="interp">
    <input type="hidden" name="interp" value="'.$_GET["dato"].'">
    <input type="text" name="valore" onchange="checkEmptyNoGPS()" id="alc">
    <input type="submit" value="Invia" disabled id="alcBtn">
</form>';}
}
else{
	echo "<h1>Dati da interpolare</h1>";
	$qry='SELECT * FROM Misure WHERE Interpolata IS NULL';
    $stmt = $database->prepare($qry);
    $stmt->execute();
    $daInterpolare=$stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($daInterpolare as $dato){
        $qry='SELECT Nome FROM Utenti WHERE Utente = :u';
        $stmt = $database->prepare($qry);
        $stmt->bindParam(":u",$dato["Utente"]);
        $stmt->execute();
        $repUser=$stmt->fetchAll()[0][0];
        echo'<a class="ui-btn ui-icon-edit ui-btn-icon-left" href="?page=dati&invia=interp&dato='.$dato["ID"].'">'.$titFrm[$dato["Tipo"].".html"].', '.$repUser.", ".date('H:i',intval($dato["Data"]))."</a>\n";
    }
    if(count($daInterpolare)==0){
        echo "&#127867;Nessun dato da interpolare, complimenti!&#127867;";
    }
}
?>