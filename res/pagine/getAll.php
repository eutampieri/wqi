<h1>Dati</h1>
<table>
    <tr>
        <th>Parametro</th>
        <th>Dato medio</th>
        <th>Dato interpolato</th>
    </tr>
<?php
    function map($val){
        if($val<=25){
            return "red";
        }
        elseif($val>35&&$val<=50){
            return "yellow";
        }
        elseif($val>50&&$val<=70){
            return "green";
        }
        elseif($val>70&&$val<=90){
            return "blue";
        }
        else{
            return "bluscuro";
        }
    }
$titFrm=json_decode(file_get_contents("res/forms.json"),true);
$units=json_decode(file_get_contents("res/units.json"),true);
$pesi=json_decode(file_get_contents("res/pesi.json"),true);
$qry="SELECT DISTINCT Tipo FROM Misure ORDER BY Tipo";
$stmt = $database->prepare($qry);
$stmt->execute();
$tipiDati=$stmt->fetchAll(PDO::FETCH_ASSOC);
$qry="SELECT * FROM Misure ORDER BY Tipo";
$stmt = $database->prepare($qry);
$stmt->execute();
$listadati=$stmt->fetchAll(PDO::FETCH_ASSOC);
$qry="SELECT * FROM Misure WHERE Interpolata IS NULL";
$stmt = $database->prepare($qry);
$stmt->execute();
$nulli=$stmt->fetchAll(PDO::FETCH_ASSOC);
$nonInterleaved=count($nulli)>0;
$dati=[];
$norm=[];
$num=[];
$wqiTot=0.0;
$titFrm=json_decode(file_get_contents("res/forms.json"),true);
$mis=json_decode(file_get_contents("res/units.json"),true);
foreach($tipiDati as $tipo){
    $dati[$tipo["Tipo"]]=0.0;
    $norm[$tipo["Tipo"]]=0.0;
    $num[$tipo["Tipo"]]=0;
}
foreach($listadati as $dato){
    $dati[$dato["Tipo"]]=$dati[$dato["Tipo"]]+floatval($dato["Misura"]);
    $norm[$dato["Tipo"]]=$norm[$dato["Tipo"]]+floatval($dato["Interpolata"]);
    $num[$dato["Tipo"]]=$num[$dato["Tipo"]]+1;
}
foreach($dati as $nome=>$dato){
    $dati[$nome]=round($dato/floatval($num[$nome]),1);
    $norm[$nome]=$norm[$nome]/floatval($num[$nome]);
    $wqiTot=$wqiTot+($norm[$nome]*$pesi[$nome]);
}
foreach($dati as $nome=>$dato){
    echo "<tr>\n<td>".$titFrm[$nome.".html"]."</td>";
    echo "<td>".str_replace(".",",",strval($dati[$nome]))." ".$mis[$nome]."</td>";
    if(isset($pesi[$nome])){
        echo "<td class=\"".map($norm[$nome])."\">".str_replace(".",",",strval($norm[$nome]))."/100</td>";
    }
    else{
        echo "<td>-</td>";
    }
    echo "</tr>";
}
?>
</table>
<?php
if($nonInterleaved){
    echo '<div class="errore">Alcuni dati non sono stati ancora interpolati</div>';
}
echo "<h3>Indice LIM:<span class=\"".map($wqiTot).'">'.str_replace(".",",",strval($wqiTot))."</span></h3>"
?>
<h4>Legenda</h4>
<div class="bluscuro">Eccellente</div>
<div class="blue">Buono</div>
<div class="green">Medio</div>
<div class="yellow">Cattivo</div>
<div class="red">Molto cattivo</div>
