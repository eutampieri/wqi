<?php
function formPath($page){
    $path="res/forms/".$page.".html";
    if(is_file($path)){
        return $path;
    }
    else{
        return "res/forms/".$page.".php";
    }
}
if(!$loggedIn){
    echo '<a class="ui-btn ui-icon-lock ui-btn-icon-left" href="?page=login">'."Sei stato disconnesso. Entra di nuovo</a>\n";
    die();
}
$qry='SELECT Autorizzazzioni FROM Utenti WHERE Utente = :u';
$stmt = $database->prepare($qry);
$stmt->bindParam(':u',$username);
$stmt->execute();
$aut=json_decode($stmt->fetchAll(PDO::FETCH_ASSOC)[0]["Autorizzazzioni"]);
if(isset($_GET["invia"])&&in_array($_GET["invia"], $aut)){
	echo '<a class="ui-btn ui-icon-carat-l ui-mini ui-btn-icon-left" href="?page=dati">Indietro</a>'."\n";
	include(formPath($_GET["invia"]));
}
else{
	echo "<h1>Invia un dato</h1>";
    //var_dump($aut);
	$titFrm=json_decode(file_get_contents("res/forms.json"),true);
	foreach(array_merge(glob("res/forms/*.html"),glob("res/forms/*.php"))as $page){
		$bnFrm=str_replace("res/forms/","",str_replace(".html", "", str_replace(".php", "", $page)));
		if(in_array($bnFrm, $aut)){
			echo '<a class="ui-btn ui-icon-edit ui-btn-icon-left" href="?page=dati&invia='.$bnFrm.'">'.$titFrm[str_replace("res/forms/","",$page)]."</a>\n";
		}
	}
    if(count($aut)==0){
        echo "Non sei stato autorizzato all'inserimento di nuovi dati, ma puoi visualizzare quelli gi&agrave; inseriti.\n"; 
    }
	echo '<a class="ui-btn ui-icon-bullets ui-btn-icon-left" href="?page=getAll">Tutti i dati</a>';
}
?>