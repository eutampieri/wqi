 

d5 gas per mm di liquido, Cioé in mg“. Tomiamd au‘assorbimcntb dcll’ossz’gcno.

ansm {momma va visto comc‘”fenomcno d1 trasifeﬁmemo di massa (fenomeno
diffusive) che tende all’equilibrio”. Questo signiﬁca die, 5c 31 esponc dcu ’ acqua priva
di ossigcno c a una cena tcmpcratura aldell‘aﬁa a una ccrta temperatura c prcssionc,
l'ossigcno si trasfcrisce daJl'aria all’acqua. .ﬁri'ma VBIOCCm’CmC’ POi 5'3um Pm
lentamcnte, fino a quando la sua concanrazionc rién ragg‘iungc '1“ V315"? OWE 31 [Q “31‘3—
essa non aumcnta piﬁ. ‘ ' I ‘

A questo punto 1c concentrazionj dell‘ossigeno dell’an'a e di QUCUO 5030110 5“
acqua 301310 in cquilibn’o tra di loro e la concentrazionc dell’ossigcno in acqua 3i
dcﬁm'scc di sarurazione con an’ai '

' In chimica tutto cit) si esprime in questo modo: '

 _ . v . . . . - . . . . . - . -> O

' (gas) (in soluzionc)

' ‘Come‘t‘utti gli equilibri’chimico-ﬁsici, anchc l'equilibrio gas/quuido presenta una
sua costdnté, Che dipcnde quasi csclusivamcntc dalla Temperature. Se L1 Temperamra

v dell’a'cqua difninuisce, l’équilibrio si sposta a‘destra e la concentrazionc dell‘ossigeno

diSCiolto aumcnta, vicevcrsa se la temperatura aumenta.

A 20°C 1a concenu-azione di.saturazione dell’acqua con l’ossjgeno dell ’aria 8: pm"
3 9,29 mg/l. ~ . '

A 10°C cssa span 21 11,39 mg/l, " , ,.

Sull‘a concentrazione dell’ossigcno discloth in 21ch agiscono 'perb altri fauori
(pressionc dell’aria, concentrazionc salina deﬂ'acqua, temperatura delL’ari-a). Tm
quhsti i1 piﬁ importante E 1a Pressione dell’aria: SC 65521 aumcnta; cresce la concentraJ
zione di ossigeno disciolto all’equiljbrio, viccversa se cssa dirninuisce.

In mon tagna per esempio, 1a concentrazione’ di 3311113110116 (1311’ acqua pub csserc.
c1211 20 a1 40% inforiorc rispetto a queﬂa presents a hveﬂo dcl mare.

E’ impommte precisare chc

non E 121 pressionc deU‘aria in 8&-

ad incidcrc sulla solubﬂité deI~

. l’ossigeno, ma la Pressione Par-

zialc dcll’ossigeno dell’aria.

‘ Poiché deﬂ’aria l'ossigeno costi~

misc: circa la quinta parts, la sua

Pressionc P317431: 6! circa un quin-

to di quclla dell’aria‘ cioé. circa
0,2 atmosferc. ~

Ricorda: la Pressionc Parzia-

le dcﬂ’ossigeno don’aria a 1 at—

. ,. . ‘ mosfera ‘e di 0,2 htmosfere, circa.

Pi‘m‘e “Guam” d“! Mme“) ‘ Si noti chc 1e ammsfcre $0110

unith di misum ancora in uso a

i : livello praLico, ma abolitc dal Sistema Internazioaalc di Unitix di Misura (SD‘ per i}

